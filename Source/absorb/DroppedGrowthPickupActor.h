// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GrowthPickupActor.h"
#include "DroppedGrowthPickupActor.generated.h"

/**
 * 
 */
UCLASS()
class ABSORB_API ADroppedGrowthPickupActor : public AGrowthPickupActor
{
	GENERATED_BODY()

private:
	class UMaterialInstance * Team1Mat;
	class UMaterialInstance * Team2Mat;

public:
	ADroppedGrowthPickupActor();
	
	virtual void HandleBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;
	virtual void Tick(float DeltaTime) override;
	virtual void BeginPlay() override;
	
	int TeamID;
	void SetTeamID(int id);
};
