// Fill out your copyright notice in the Description page of Project Settings.


#include "GrowthPickupActor.h"

#include <UObject/ConstructorHelpers.h>
#include <Components/StaticMeshComponent.h>
#include <Components/BillboardComponent.h>
#include <Materials/MaterialInstance.h>

#include "OrbPawn.h"

AGrowthPickupActor::AGrowthPickupActor() : Super()
{
	static ConstructorHelpers::FObjectFinder<UStaticMesh> Mesh(TEXT("/Engine/BasicShapes/Sphere"));
	if (Mesh.Succeeded())
	{
		Slime = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Growth Orb"));
		Slime->SetStaticMesh(Mesh.Object);
		static ConstructorHelpers::FObjectFinder<UMaterialInstance> Mat(TEXT("/Game/Textures/GrowthPickup"));
		if (Mat.Succeeded()) Slime->SetMaterial(0, Mat.Object);
		Slime->SetupAttachment(RootComponent);
		Slime->SetRelativeScale3D(FVector(2.0f));
		Slime->SetRelativeLocation(FVector(0.0f, 0.0f, -100.0f));
		Slime->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
	static ConstructorHelpers::FObjectFinder<UTexture2D> Sprite(TEXT("/Game/Textures/Growth"));
	if (Sprite.Succeeded()) Billboard->SetSprite(Sprite.Object);
}

void AGrowthPickupActor::Apply(AOrbPawn * OrbPawn)
{
	OrbPawn->GrowSphere(0.05f, 1.0f);

	Destroy();
}

void AGrowthPickupActor::Unapply()
{
}

void AGrowthPickupActor::RunTimer()
{
}
