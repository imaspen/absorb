// Fill out your copyright notice in the Description page of Project Settings.


#include "AbsorbGameStateBase.h"
#include <Engine/Classes/Kismet/GameplayStatics.h>
#include <Engine/World.h>
#include <Kismet/GameplayStatics.h>
#include <GameFramework/PlayerState.h>

AAbsorbGameStateBase::AAbsorbGameStateBase()
{
	WinScore = 5.0f;
}

void AAbsorbGameStateBase::Tick(float DeltaTime) {
	Super::Tick(DeltaTime); 
}


float AAbsorbGameStateBase::GetTeam1Score()
{
	return Team1Score;
}

void AAbsorbGameStateBase::SetTeam1Score()
{
	Team1Score += 0.05f;
	if (Team1Score >= WinScore) LoadScoreScreen();	
}

float AAbsorbGameStateBase::GetTeam2Score()
{
	return Team2Score;
}

void AAbsorbGameStateBase::SetTeam2Score()
{
	Team2Score += 0.05f;
	if (Team2Score >= WinScore) LoadScoreScreen();
}

void AAbsorbGameStateBase::LoadScoreScreen()
{
	for (int i = 0; i < PlayerArray.Num(); i++)
	{
		UGameplayStatics::RemovePlayer((APlayerController *)PlayerArray[i]->GetOwner(), true);
	}
	UGameplayStatics::OpenLevel(GetWorld(), "/Game/CreditScreen");
}
