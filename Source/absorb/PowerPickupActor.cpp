// Fill out your copyright notice in the Description page of Project Settings.


#include "PowerPickupActor.h"
#include "TimerManager.h"
#include "OrbPawn.h"
#include <UObject/ConstructorHelpers.h>
#include <Particles/ParticleSystemComponent.h>
#include <Components/BillboardComponent.h>

APowerPickupActor::APowerPickupActor() : Super()
{
	static ConstructorHelpers::FObjectFinder<UParticleSystem> Particle(TEXT("/Game/Assets/Pickups4"));
	if (Particle.Succeeded())
	{
		ParticleSystem->SetTemplate(Particle.Object);
	}
	static ConstructorHelpers::FObjectFinder<UTexture2D> Sprite(TEXT("/Game/Textures/Power"));
	if (Sprite.Succeeded()) Billboard->SetSprite(Sprite.Object);
}
void APowerPickupActor::Apply(AOrbPawn * OrbPawn)
{
	OrbPawn->SetPowerModifier(2.0f);
	AffectedOrb = OrbPawn;
	RunTimer();
}
void APowerPickupActor::RunTimer() {
	FTimerHandle PickupTimerHandle;
	GetWorldTimerManager().SetTimer(PickupTimerHandle, this, &APowerPickupActor::Unapply, 20.0f, false, -1.0f);
}

void APowerPickupActor::Unapply()
{
	AffectedOrb->SetPowerModifier(1.0f);
	Destroy();
}
