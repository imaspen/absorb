// Fill out your copyright notice in the Description page of Project Settings.


#include "absorbGameModeBase.h"

#include <Kismet/GameplayStatics.h>
#include <Engine/World.h>
#include <GameFramework/PlayerStart.h>

#include "OrbPlayerState.h"

void AabsorbGameModeBase::BeginPlay()
{
	UGameplayStatics::CreatePlayer(GetWorld());
	UGameplayStatics::CreatePlayer(GetWorld());
	UGameplayStatics::CreatePlayer(GetWorld());
}

AActor * AabsorbGameModeBase::ChoosePlayerStart_Implementation(AController * Player)
{
	if (_playerStarts.Num() == 0)
	{
		UGameplayStatics::GetAllActorsOfClass(
			GetWorld(), APlayerStart::StaticClass(), _playerStarts
		);
	}
	if (_playerStarts.Num() == 0) return nullptr;
	auto playerStart = Cast<APlayerStart, AActor>(_playerStarts.Pop());
	auto playerState = Player->GetPlayerState<AOrbPlayerState>();
	if (playerState == nullptr) return nullptr;
	playerState->SetTeamID(playerStart->PlayerStartTag == FName(TEXT("Team 1")) ? 1 : 2);
	return playerStart;
}
