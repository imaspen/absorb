// Fill out your copyright notice in the Description page of Project Settings.


#include "OrbPawn.h"

#include <Components/SphereComponent.h>
#include <Components/StaticMeshComponent.h>
#include <ConstructorHelpers.h>
#include <Materials/MaterialInstance.h>

#include "OrbPlayerState.h"
#include "PickupActor.h"
#include "OrbPlayerController.h"
#include "OrbPlayerState.h"
#include "DroppedGrowthPickupActor.h"

// Sets default values
AOrbPawn::AOrbPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));
	CollisionSphere->SetSimulatePhysics(true);
	CollisionSphere->SetEnableGravity(true);
	CollisionSphere->SetMassOverrideInKg(NAME_None, 100.0f);
	CollisionSphere->SetSphereRadius(50.0f);
	CollisionSphere->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel1);
	CollisionSphere->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	CollisionSphere->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	CollisionSphere->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel1, ECollisionResponse::ECR_Overlap);
	CollisionSphere->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel2, ECollisionResponse::ECR_Overlap);
	RootComponent = CollisionSphere;

	RenderSphere = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Render Component"));
	RenderSphere->SetRelativeScale3D(FVector(2.0f));
	RenderSphere->SetGenerateOverlapEvents(false);
	RenderSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	RenderSphere->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SphereVisualAsset(TEXT("/Engine/BasicShapes/Sphere"));
	if (SphereVisualAsset.Succeeded())
	{
		RenderSphere->SetStaticMesh(SphereVisualAsset.Object);
	}
	RenderSphere->SetupAttachment(RootComponent);

    SpeedModifier = 1.0f;
	JumpModifier = 1.0f;
	PowerModifier = 1.0f;

	_targetScale = 1.0f;
	_isGrowing = false;

	_isGrounded = true;
	bTryJump = false;
}

void AOrbPawn::SetSize_Internal(float Size)
{
	CollisionSphere->SetRelativeScale3D(FVector(Size));
	float newMass = 80.0f + 4.0f * FMath::Pow(5, Size);
	CollisionSphere->SetMassOverrideInKg(NAME_None, newMass);
}

#pragma region Tick Handlers
void AOrbPawn::HandleMovement()
{
	FVector movement = ConsumeMovementInputVector();
	if (movement.IsNearlyZero())
	{
		CollisionSphere->SetAngularDamping(1.0f);
		return;
	}
	CollisionSphere->SetAngularDamping(0.0f);
	CollisionSphere->AddForce((GetControlRotation().RotateVector(movement.GetSafeNormal()) * 100000.0f) * SpeedModifier);
}

void AOrbPawn::HandleJump()
{
	if (!bTryJump) return;
	if (!IsGrounded()) return;

	bTryJump = false;
	_isGrounded = false;
	CollisionSphere->AddImpulse(FVector(0.0f, 0.0f, 50000.0f * JumpModifier));
}



void AOrbPawn::HandleGrow(float DeltaTime)
{
	if (!_isGrowing) return;

	_growTime += DeltaTime;
	float newScale = FMath::Lerp<float, float>(_startScale, _targetScale, _growTime);

	if (newScale > _targetScale)
	{
		_isGrowing = false;
		newScale = _targetScale;
	}

	SetSize_Internal(newScale);
}

void AOrbPawn::HandleSuck(float DeltaTime)
{
	TArray<AActor *> overlaps;
	GetOverlappingActors(overlaps, AOrbPawn::StaticClass());
	for (auto & otherOrb : overlaps)
	{
		auto pawn = Cast<AOrbPawn, AActor>(otherOrb);
		if (pawn == nullptr) return;
		auto otherState = pawn->GetPlayerState<AOrbPlayerState>();
		if (otherState == nullptr) return;
		if (otherState->GetTeamID() == GetPlayerState<AOrbPlayerState>()->GetTeamID()) return;
        if (Cast<AOrbPawn, AActor>(otherOrb)->GetPower()
			< GetPower())
		{
			SetSize_Internal(GetActorRelativeScale3D().X + 0.01f);
			_targetScale += 0.01f;
		}
		else
		{
			SetSize_Internal(GetActorRelativeScale3D().X - 0.01f);
			_targetScale -= 0.01f;
			if (CollisionSphere->GetScaledSphereRadius() < 25.0f)
			{
				Destroy();
			}
		}
	}
}

void AOrbPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	HandleMovement();
	HandleJump();
	HandleGrow(DeltaTime);
	HandleSuck(DeltaTime);
	CheckSize();
	ShrinkIfTooBig();
}
#pragma endregion

bool AOrbPawn::IsGrounded()
{
	FHitResult hit;
	FVector start = GetActorLocation();
	FVector end = start;
	end.Z -= CollisionSphere->GetScaledSphereRadius();
	end.Z -= 1.0f;
	FCollisionQueryParams params;
	FCollisionResponseParams responseParams;

	GetWorld()->LineTraceSingleByChannel(hit, start, end, ECollisionChannel::ECC_GameTraceChannel1, params, responseParams);
	return hit.bBlockingHit;
}

// Called when the game starts or when spawned
void AOrbPawn::BeginPlay()
{
	Super::BeginPlay();
	
	/*auto playerState = GetPlayerStateChecked<AOrbPlayerState>();
	playerState->TeamID = 1;*/
}

// Called to bind functionality to insput
void AOrbPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AOrbPawn::UpdateTeam(int TeamID)
{
	RenderSphere->SetMaterial(0, TeamID == 1 ? TeamOneMaterial : TeamTwoMaterial);
}

void AOrbPawn::GrowSphere(float TargetScale, float GrowthDuration)
{
	_startScale = CollisionSphere->GetRelativeTransform().GetScale3D().X;
	_targetScale += TargetScale;
	_growTime = 0.0f;
	_isGrowing = true;
}

void AOrbPawn::ShrinkIfTooBig() {
	if (TooBig != 0.0f) {
		GrowSphere(-0.005f, 0.01f);
	}
}

void AOrbPawn::SetSize(float Size)
{
	_targetScale = Size;
	SetSize_Internal(Size);
}

void AOrbPawn::CheckSize() {
	auto size = CollisionSphere->GetScaledSphereRadius();
	if (size > 100.0f && size != 0.0f) {
		TooBig = size;
	} 
	if(size < 75.0f){
		if (TooBig != 0.0f)
		{
			FTransform * transform = new FTransform(GetTransform());
			transform->SetRotation(FQuat(0.0f, 0.0f, 0.0f, 0.0f));
			FActorSpawnParameters params;
			_currentPickup = GetWorld()->SpawnActor(
				ADroppedGrowthPickupActor::StaticClass(), transform, params
			);
			auto pickup = Cast<ADroppedGrowthPickupActor, AActor>(_currentPickup);
			pickup->SetTeamID(GetPlayerState<AOrbPlayerState>()->GetTeamID());
			pickup->CollisionSphere->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel1, ECollisionResponse::ECR_Overlap);
			TooBig = 0.0f;
		}
	}
}

float AOrbPawn::GetPower() {
	return CollisionSphere->GetScaledSphereRadius()*PowerModifier;
}

void AOrbPawn::SetPowerModifier(float Modifier) {
	PowerModifier = Modifier;
}