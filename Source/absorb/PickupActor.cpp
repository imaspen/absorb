// Fill out your copyright notice in the Description page of Project Settings.


#include "PickupActor.h"

#include <Components/SphereComponent.h>
#include <Components/BillboardComponent.h>
#include <Particles/ParticleSystemComponent.h>

#include "OrbPawn.h"

// Sets default values
APickupActor::APickupActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CollisionSphere = CreateDefaultSubobject<USphereComponent>("Collision Sphere");
	CollisionSphere->SetMobility(EComponentMobility::Stationary);
	CollisionSphere->SetSphereRadius(50.0f);
	CollisionSphere->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	CollisionSphere->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel2);
	CollisionSphere->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	CollisionSphere->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel1, ECollisionResponse::ECR_Overlap);
	CollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &APickupActor::HandleBeginOverlap);
	RootComponent = CollisionSphere;

	ParticleSystem = CreateDefaultSubobject<UParticleSystemComponent>("Visual Sphere");
	ParticleSystem->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	ParticleSystem->SetMobility(EComponentMobility::Stationary);
	ParticleSystem->SetupAttachment(CollisionSphere);

	Billboard = CreateDefaultSubobject<UBillboardComponent>("Billboard");
	Billboard->SetRelativeScale3D(FVector(0.25f));
	Billboard->SetupAttachment(ParticleSystem);
}

void APickupActor::HandleBeginOverlap(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	AOrbPawn * player = Cast<AOrbPawn, AActor>(OtherActor);
	if (player == nullptr) return;
	CollisionSphere->SetVisibility(false, true);
	CollisionSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Apply(player);
}

// Called when the game starts or when spawned
void APickupActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APickupActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

