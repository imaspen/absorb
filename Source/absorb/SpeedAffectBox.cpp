// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedAffectBox.h"
#include "OrbPawn.h"
#include <Components/SphereComponent.h>
#include "Engine/Engine.h"

void ASpeedAffectBox::OnOverlap(AOrbPawn * OrbPawn)
{
	FVector SpeedVector = Impulse * 100000.0f;
	SpeedVector = GetActorQuat() * SpeedVector;
	OrbPawn->CollisionSphere->AddImpulse(SpeedVector);
}
 