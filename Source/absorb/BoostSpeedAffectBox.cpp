// Fill out your copyright notice in the Description page of Project Settings.


#include "BoostSpeedAffectBox.h"

#include <Components/StaticMeshComponent.h>
#include <UObject/ConstructorHelpers.h>
#include <Materials/Material.h>

ABoostSpeedAffectBox::ABoostSpeedAffectBox() : Super()
{
	CubeMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Cube Mesh"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> Cube(TEXT("/Engine/BasicShapes/Cube"));
	if (Cube.Succeeded())
	{
		CubeMesh->SetStaticMesh(Cube.Object);
		CubeMesh->SetRelativeLocation(FVector(0.0f, 0.0f, -44.0f));
		CubeMesh->SetRelativeScale3D(FVector(1.25f, 1.25f, 0.0575f));
	}
	static ConstructorHelpers::FObjectFinder<UMaterial> Mat(TEXT("/Game/VFX/NewMaterial"));
	if (Mat.Succeeded())
	{
		CubeMesh->SetMaterial(0, Mat.Object);
	}
	CubeMesh->SetupAttachment(RootComponent);
	CubeMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}
