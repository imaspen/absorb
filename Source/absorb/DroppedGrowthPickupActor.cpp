// Fill out your copyright notice in the Description page of Project Settings.


#include "DroppedGrowthPickupActor.h"

#include <Components/SphereComponent.h>
#include <Components/StaticMeshComponent.h>
#include <Materials/MaterialInstance.h>
#include <UObject/ConstructorHelpers.h>

#include "OrbPawn.h"
#include "OrbPlayerState.h"

ADroppedGrowthPickupActor::ADroppedGrowthPickupActor() : Super()
{
	RootComponent->SetMobility(EComponentMobility::Movable);
	static ConstructorHelpers::FObjectFinder<UMaterialInstance> Mat1(TEXT("/Game/Textures/Team_1_"));
	if (Mat1.Succeeded()) Team1Mat = Mat1.Object;
	static ConstructorHelpers::FObjectFinder<UMaterialInstance> Mat2(TEXT("/Game/Textures/Team_1_1"));
	if (Mat2.Succeeded()) Team2Mat = Mat2.Object;
	Slime->SetRelativeLocation(FVector(0.0f, 0.0f, -75.0f));
}

void ADroppedGrowthPickupActor::HandleBeginOverlap(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	AOrbPawn * player = Cast<AOrbPawn, AActor>(OtherActor);
	if (player == nullptr) return;
	auto playerState = player->GetPlayerState<AOrbPlayerState>();
	if (playerState == nullptr) return;
	if (playerState->GetTeamID() == TeamID) return; 
	CollisionSphere->SetVisibility(false, true);
	CollisionSphere->SetSimulatePhysics(false);
	CollisionSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Apply(player);
}

void ADroppedGrowthPickupActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ADroppedGrowthPickupActor::BeginPlay()
{
	CollisionSphere->BodyInstance.bLockXRotation = true;
	CollisionSphere->BodyInstance.bLockYRotation = true;
	CollisionSphere->BodyInstance.bLockZRotation = true;
	CollisionSphere->BodyInstance.bLockXTranslation = true;
	CollisionSphere->BodyInstance.bLockYTranslation = true;
	CollisionSphere->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
	CollisionSphere->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldStatic, ECollisionResponse::ECR_Block);
	CollisionSphere->SetSimulatePhysics(true);
}

void ADroppedGrowthPickupActor::SetTeamID(int id)
{
	TeamID = id;
	Slime->SetMaterial(0, id == 1 ? Team1Mat : Team2Mat);
}
