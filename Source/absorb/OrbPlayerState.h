// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "OrbPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class ABSORB_API AOrbPlayerState : public APlayerState
{
	GENERATED_BODY()
	
private:
	int _teamID;

public:
	UFUNCTION(BlueprintCallable)
	int GetTeamID();

	UFUNCTION(BlueprintCallable)
	void SetTeamID(int newID);
};
