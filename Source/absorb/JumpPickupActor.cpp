// Fill out your copyright notice in the Description page of Project Settings.


#include "JumpPickupActor.h"

#include <TimerManager.h>
#include <UObject/ConstructorHelpers.h>
#include <Particles/ParticleSystemComponent.h>
#include <Components/BillboardComponent.h>

#include "OrbPawn.h"

AJumpPickupActor::AJumpPickupActor() : Super()
{
	static ConstructorHelpers::FObjectFinder<UParticleSystem> Particle(TEXT("/Game/Assets/Pickups3"));
	if (Particle.Succeeded())
	{
		ParticleSystem->SetTemplate(Particle.Object);
	}
	static ConstructorHelpers::FObjectFinder<UTexture2D> Sprite(TEXT("/Game/Textures/Jump"));
	if (Sprite.Succeeded()) Billboard->SetSprite(Sprite.Object);
}

void AJumpPickupActor::Apply(AOrbPawn * OrbPawn)
{
	OrbPawn->JumpModifier = 3.0f;
	AffectedOrb = OrbPawn;
	RunTimer();

}

void AJumpPickupActor::RunTimer()
{
	FTimerHandle PickupTimerHandle;
	GetWorldTimerManager().SetTimer(PickupTimerHandle, this, &AJumpPickupActor::Unapply, 1.0f, true, 5.0f);

}

void AJumpPickupActor::Unapply()
{
	AffectedOrb->JumpModifier = 1.0f;
	Destroy();
}
