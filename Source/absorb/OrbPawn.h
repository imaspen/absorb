// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "OrbPawn.generated.h"

UCLASS()
class ABSORB_API AOrbPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AOrbPawn();

private:
	bool _isGrowing;
	float _startScale;
	float _targetScale;
	float _growTime;

	bool _isGrounded;

	void HandleMovement();
	
	void HandleJump();



	void HandleGrow(float DeltaTime);

	void HandleSuck(float DeltaTime);

	bool IsGrounded();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	void SetSize_Internal(float Size);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void UpdateTeam(int TeamID);

	void HandlePower();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class USphereComponent * CollisionSphere;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UStaticMeshComponent * RenderSphere;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	class UMaterialInstance * TeamOneMaterial;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	class UMaterialInstance * TeamTwoMaterial;

	UFUNCTION(BlueprintCallable)
	virtual void GrowSphere(float TargetScale, float GrowthDuration);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float SpeedModifier;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float JumpModifier;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float PowerModifier;


	/*UFUNCTION()
	void OnOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);*/

	bool bTryJump;

	float Power;
	float TooBig;
	void SetSize(float Size);

	void CheckSize();
	void ShrinkIfTooBig();
	void SetPowerModifier(float Modifier);
	float GetPower();
	AActor * _currentPickup;
};
