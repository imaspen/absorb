// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AffectBox.h"
#include "SpeedAffectBox.generated.h"

/**
 * 
 */
UCLASS(Abstract)
class ABSORB_API ASpeedAffectBox : public AAffectBox
{
	GENERATED_BODY()
	
public:
	virtual void OnOverlap(AOrbPawn * OrbPawn) override;
	UPROPERTY(EditAnywhere)
	FVector Impulse;
 };
