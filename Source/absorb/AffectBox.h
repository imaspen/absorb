// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "AffectBox.generated.h"

UCLASS(Abstract)
class ABSORB_API AAffectBox : public ATriggerBox
{
	GENERATED_BODY()

public:
	AAffectBox();
	class UBoxComponent * CollisionCube;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UStaticMeshComponent * MeshComponent;


private:
	UFUNCTION()
	void HandleBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

public:
	virtual void Tick(float DeltaTime) override;
	virtual void OnOverlap(class AOrbPawn * OrbPawn) PURE_VIRTUAL(AAffectBox::OnOverlap,);
};
