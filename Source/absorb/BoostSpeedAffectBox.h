// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SpeedAffectBox.h"
#include "BoostSpeedAffectBox.generated.h"

/**
 * 
 */
UCLASS()
class ABSORB_API ABoostSpeedAffectBox : public ASpeedAffectBox
{
	GENERATED_BODY()
	
public:
	ABoostSpeedAffectBox();

	UPROPERTY(EditAnywhere)
	class UStaticMeshComponent * CubeMesh;
};
