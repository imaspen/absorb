// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "AbsorbGameStateBase.generated.h"

/**
 * 
 */
UCLASS()
class ABSORB_API AAbsorbGameStateBase : public AGameStateBase
{
	GENERATED_BODY()
	
public:
	AAbsorbGameStateBase();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Team1Score;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Team2Score;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float WinScore;







	 void Tick(float DeltaTime) override;
	
	 float GetTeam1Score();
	 void SetTeam1Score();
	 float GetTeam2Score();
	 void SetTeam2Score();
	 void LoadScoreScreen();

};
