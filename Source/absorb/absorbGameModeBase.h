// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "absorbGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ABSORB_API AabsorbGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
private:
	TArray<AActor *> _playerStarts;

public:
	virtual void BeginPlay() override;
	virtual AActor * ChoosePlayerStart_Implementation(AController * Player) override;
};
