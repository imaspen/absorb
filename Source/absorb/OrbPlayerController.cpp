// Fill out your copyright notice in the Description page of Project Settings.


#include "OrbPlayerController.h"

#include <GameFramework/Pawn.h>
#include <Engine/World.h>
#include <TimerManager.h>
#include <GameFramework/PlayerStart.h>

#include "OrbPawn.h"
#include "OrbPlayerState.h"
#include "AbsorbGameStateBase.h"
#include "absorbGameModeBase.h"

void AOrbPlayerController::MoveForward(float AxisValue)
{
	auto pawn = GetPawn();
	if (pawn == nullptr) return;
	pawn->AddMovementInput(FVector(1.0, 0.0f, 0.0f), AxisValue);
}

void AOrbPlayerController::MoveRight(float AxisValue)
{
	auto pawn = GetPawn();
	if (pawn == nullptr) return;
	pawn->AddMovementInput(FVector(0.0, 1.0f, 0.0f), AxisValue);
}

void AOrbPlayerController::RotateCameraY(float AxisValue)
{
	FRotator newRotation = GetControlRotation().Add(AxisValue, 0.0f, 0.0f);
	newRotation.Pitch = FMath::Clamp<float>(newRotation.Pitch, -45.0f, 20.0f);
	SetControlRotation(newRotation);
}

void AOrbPlayerController::RotateCameraX(float AxisValue)
{
	SetControlRotation(GetControlRotation().Add(0.0f, AxisValue, 0.0f));
}

void AOrbPlayerController::StartJump()
{
	AOrbPawn * OrbPawn = Cast<AOrbPawn, APawn>(GetPawn());
	if (OrbPawn == nullptr) return;
	OrbPawn->bTryJump = true;
}

void AOrbPlayerController::EndJump()
{
	AOrbPawn * OrbPawn = Cast<AOrbPawn, APawn>(GetPawn());
	if (OrbPawn == nullptr) return;
	OrbPawn->bTryJump = false;
}

void AOrbPlayerController::MakeNewBoy()
{
	auto gameMode = GetWorld()->GetAuthGameMode();
	if (gameMode == nullptr) return;
	auto pawnClass = gameMode->GetDefaultPawnClassForController(this);
	auto playerStart = Cast<APlayerStart, AActor>(gameMode->FindPlayerStart(this));
	gameMode->RestartPlayerAtPlayerStart(this, playerStart);
}

void AOrbPlayerController::SetupInputComponent()
{
	if (InputComponent == NULL)
	{
		InputComponent = NewObject<UInputComponent>(this, TEXT("PC_InputComponent0"));
		InputComponent->RegisterComponent();
	}

	InputComponent->BindAxis("MoveForward", this, &AOrbPlayerController::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AOrbPlayerController::MoveRight);
	InputComponent->BindAxis("RotateCameraX", this, &AOrbPlayerController::RotateCameraX);
	InputComponent->BindAxis("RotateCameraY", this, &AOrbPlayerController::RotateCameraY);

	InputComponent->BindAction("Jump", EInputEvent::IE_Pressed, this, &AOrbPlayerController::StartJump);
	InputComponent->BindAction("Jump", EInputEvent::IE_Released, this, &AOrbPlayerController::EndJump);
}

void AOrbPlayerController::OnPossess(APawn * InPawn)
{
	Super::OnPossess(InPawn);

	auto orbPawn = Cast<AOrbPawn, APawn>(InPawn);
	if (orbPawn == nullptr) return;
	orbPawn->UpdateTeam(
		Cast<AOrbPlayerState, APlayerState>(PlayerState)->GetTeamID()
	);
}

void AOrbPlayerController::OnUnPossess()
{
	Super::OnUnPossess();
	FTimerHandle timerHandle;
	GetWorld()->GetTimerManager().SetTimer(
		timerHandle, this, &AOrbPlayerController::MakeNewBoy, 5.0f, false
	);
}
