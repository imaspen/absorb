// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickupActor.h"
#include "GrowthPickupActor.generated.h"

/**
 * 
 */
UCLASS()
class ABSORB_API AGrowthPickupActor : public APickupActor
{
	GENERATED_BODY()

public:
	AGrowthPickupActor();

	virtual void Apply(class AOrbPawn * OrbPawn) override;
	virtual void Unapply() override;
	virtual void RunTimer() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	class UStaticMeshComponent * Slime;
};
