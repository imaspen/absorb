// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickupActor.h"
#include "PowerPickupActor.generated.h"

/**
 * 
 */
UCLASS()
class ABSORB_API APowerPickupActor : public APickupActor
{
	GENERATED_BODY()
	
public: 
	APowerPickupActor();
	virtual void RunTimer() override;
	virtual void Apply(class AOrbPawn * OrbPawn) override;
	virtual void Unapply() override;
};
