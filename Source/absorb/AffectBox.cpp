// Fill out your copyright notice in the Description page of Project Settings.


#include "AffectBox.h"
#include "Components/BoxComponent.h"
#include "OrbPawn.h"
#include "Components/StaticMeshComponent.h"
#include <Components/BillboardComponent.h>
#include <Components/ShapeComponent.h>
#include <UObject/ConstructorHelpers.h>
#include <Materials/Material.h>

AAffectBox::AAffectBox()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CollisionCube = Cast<UBoxComponent, UShapeComponent>(GetCollisionComponent());
	CollisionCube->SetBoxExtent(FVector(50.0f));
	CollisionCube->SetMobility(EComponentMobility::Stationary);
	CollisionCube->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	CollisionCube->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel2);
	CollisionCube->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	CollisionCube->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel1, ECollisionResponse::ECR_Overlap);
	CollisionCube->OnComponentBeginOverlap.AddDynamic(this, &AAffectBox::HandleBeginOverlap);

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Pad"));
	MeshComponent->SetRelativeLocation(FVector(0.0f));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> PadMesh(TEXT("/Game/Jump_Pad"));
	if (PadMesh.Succeeded())
	{
		MeshComponent->SetStaticMesh(PadMesh.Object);
		MeshComponent->SetWorldRotation(FRotator(0.0f, 0.0f, 90.0f));
		MeshComponent->SetRelativeScale3D(FVector(2.25f));
		MeshComponent->SetRelativeLocation(FVector(0.0f, 0.0f, -50.0f));
	}	
	static ConstructorHelpers::FObjectFinder<UMaterial> PadMat(TEXT("/Game/Assets/NewMaterial"));
	if (PadMat.Succeeded())
	{
		MeshComponent->SetMaterial(0, PadMat.Object);
	}
	MeshComponent->SetupAttachment(RootComponent);
	SetActorHiddenInGame(false);
	GetCollisionComponent()->SetHiddenInGame(true);
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}
void AAffectBox::HandleBeginOverlap(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	AOrbPawn * player = Cast<AOrbPawn, AActor>(OtherActor);
	if (player == nullptr) return;
	OnOverlap(player);
}

// Called every frame
void AAffectBox::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
