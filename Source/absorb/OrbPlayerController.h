// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "OrbPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class ABSORB_API AOrbPlayerController : public APlayerController
{
	GENERATED_BODY()
	
private:
	void MoveForward(float AxisValue);
	void MoveRight(float AxisValue);
	void RotateCameraY(float AxisValue);
	void RotateCameraX(float AxisValue);

	void StartJump();
	void EndJump();

	UFUNCTION()
	void MakeNewBoy();

public:
	virtual void SetupInputComponent() override;

	virtual void OnPossess(APawn * InPawn) override;
	virtual void OnUnPossess() override;
};
