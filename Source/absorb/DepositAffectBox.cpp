// Fill out your copyright notice in the Description page of Project Settings.


#include "DepositAffectBox.h"
#include "Orbpawn.h"
#include "AbsorbGameStateBase.h"
#include <Components/SphereComponent.h>
#include "OrbPlayerState.h"
void ADepositAffectBox::OnOverlap(AOrbPawn * OrbPawn) {
	
}

void ADepositAffectBox::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	SetSize();

}  

void ADepositAffectBox::SetSize() {
	TArray<AActor *> overlaps;
	GetOverlappingActors(overlaps, AOrbPawn::StaticClass());
	for (auto & otherOrb : overlaps)
	{
		auto orbPawn = Cast<AOrbPawn, AActor>(otherOrb);
		if (orbPawn->CollisionSphere->GetScaledSphereRadius() > 55.0f) {
			auto orbTeam = orbPawn->GetPlayerState<AOrbPlayerState>()->GetTeamID();
			if (TeamID != orbTeam) return;
			orbPawn->SetSize(orbPawn->GetActorRelativeScale3D().X - 0.01f);
			if (orbTeam == 1) {
				GetWorld()->GetGameState<AAbsorbGameStateBase>()->SetTeam1Score();
			}
			else {
				GetWorld()->GetGameState<AAbsorbGameStateBase>()->SetTeam2Score();
			}
		}
	}
}
