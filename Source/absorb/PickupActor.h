// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PickupActor.generated.h"

UCLASS(Abstract)
class ABSORB_API APickupActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickupActor();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class USphereComponent * CollisionSphere;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UParticleSystemComponent * ParticleSystem;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UBillboardComponent * Billboard;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class AOrbPawn * AffectedOrb;

	UFUNCTION()
   virtual void HandleBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void Apply(class AOrbPawn * OrbPawn) PURE_VIRTUAL(APickupActor::Apply,);
	virtual void Unapply() PURE_VIRTUAL(APickupActor::Unapply, );
	virtual void RunTimer() PURE_VIRTUAL(APickupActor::RunTimer,);
};
