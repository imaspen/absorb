// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickupActor.h"
#include "SpeedPickupActor.generated.h"

/**
 * 
 */
UCLASS()
class ABSORB_API ASpeedPickupActor : public APickupActor
{
	GENERATED_BODY()

public:
	ASpeedPickupActor();

	virtual void RunTimer() override;
	virtual void Apply(class AOrbPawn * OrbPawn) override; 
	virtual void Unapply() override;
}; 
