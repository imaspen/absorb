// Fill out your copyright notice ` the Description page of Project Settings.


#include "PickupSpawnerActor.h"

#include <Engine/World.h>
#include <UObject/ConstructorHelpers.h>
#include <Components/BillboardComponent.h>
#include <TimerManager.h>

#include "PickupActor.h"

// Sets default values
APickupSpawnerActor::APickupSpawnerActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	UBillboardComponent * sprite = CreateDefaultSubobject<UBillboardComponent>(TEXT("Sprite"));
	static ConstructorHelpers::FObjectFinder<UTexture2D> newAsset(TEXT("/Engine/EditorResources/S_Trigger"));
	if (newAsset.Succeeded())
	{
		sprite->SetSprite(newAsset.Object);
		sprite->SetRelativeLocation(FVector(0.0f, 0.0f, 60.0f));
		sprite->SetHiddenInGame(true);
		sprite->MarkAsEditorOnlySubobject();
	}
	sprite->SetupAttachment(RootComponent);
}

void APickupSpawnerActor::PickupDestroyed(AActor * Destroyed)
{
	GetWorldTimerManager().SetTimer(
		_spawnCooldownHandle, this, &APickupSpawnerActor::SpawnPickup, RespawnDelay, false
	);
}

// Called when the game starts or when spawned
void APickupSpawnerActor::BeginPlay()
{
	Super::BeginPlay();
	
	SpawnPickup();
}

// Called every frame
void APickupSpawnerActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APickupSpawnerActor::SpawnPickup()
{
	FTransform * transform = new FTransform(GetTransform());
	transform->AddToTranslation(FVector(0.0f, 0.0f, 75.0f));
	FActorSpawnParameters params;

	_currentPickup = GetWorld()->SpawnActor(
		*Pickup,
		transform,
		params
	);

	if (_currentPickup == nullptr)
	{
		PickupDestroyed(nullptr);
		return;
	}

	_currentPickup->OnDestroyed.AddDynamic(this, &APickupSpawnerActor::PickupDestroyed);
}

