// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AffectBox.h"
#include "DepositAffectBox.generated.h"

/**
 * 
 */
UCLASS()
class ABSORB_API ADepositAffectBox : public AAffectBox
{
	GENERATED_BODY()
public:
	virtual void OnOverlap(AOrbPawn * OrbPawn) override;
	virtual void Tick(float DeltaTime) override;
	virtual void SetSize();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int TeamID;
};
