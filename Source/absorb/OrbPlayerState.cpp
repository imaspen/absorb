// Fill out your copyright notice in the Description page of Project Settings.


#include "OrbPlayerState.h"

#include "OrbPawn.h"

int AOrbPlayerState::GetTeamID()
{
	return _teamID;
}

void AOrbPlayerState::SetTeamID(int NewID)
{
	_teamID = NewID;
}
