// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedPickupActor.h"
#include "TimerManager.h"
#include "OrbPawn.h"
#include <UObject/ConstructorHelpers.h>
#include <Particles/ParticleSystemComponent.h>
#include <Components/BillboardComponent.h>

ASpeedPickupActor::ASpeedPickupActor() : Super()
{
	static ConstructorHelpers::FObjectFinder<UParticleSystem> Particle(TEXT("/Game/Assets/Pickups2"));
	if (Particle.Succeeded())
	{
		ParticleSystem->SetTemplate(Particle.Object);
	}
	static ConstructorHelpers::FObjectFinder<UTexture2D> Sprite(TEXT("/Game/Textures/Speed"));
	if (Sprite.Succeeded()) Billboard->SetSprite(Sprite.Object);
}

void ASpeedPickupActor::Apply(AOrbPawn * OrbPawn)
{
	OrbPawn->SpeedModifier = 2.0f; 
	AffectedOrb = OrbPawn;
	RunTimer();
}

void ASpeedPickupActor::RunTimer()
{
	FTimerHandle PickupTimerHandle;
	GetWorldTimerManager().SetTimer(PickupTimerHandle, this, &ASpeedPickupActor::Unapply, 5.0f, false, -1.0f);
}

void ASpeedPickupActor::Unapply()
{
	AffectedOrb->SpeedModifier = 1.0f;
	Destroy();
}