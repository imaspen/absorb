// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PickupSpawnerActor.generated.h"

UCLASS()
class ABSORB_API APickupSpawnerActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickupSpawnerActor();

private:
	FTimerHandle _spawnCooldownHandle;

	UFUNCTION()
	void PickupDestroyed(class AActor * Destroyed);

	AActor * _currentPickup;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void SpawnPickup();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<class APickupActor> Pickup;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float RespawnDelay;
};
